package mx.rey.solution.empleados.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import mx.rey.solution.empleados.domain.Sessions;
import mx.rey.solution.empleados.service.IConsultaService;
import mx.rey.solution.empleados.service.IEmpleadosService; 

@RestController
@RequestMapping("/empleados")
public class EmpleadosController {
 
	@Autowired
	private IEmpleadosService empleadosService;
	
	@Autowired
	private IConsultaService iConsultaService;
 
//	@ApiOperation(value = "Consulta de datos del empleados.", notes = "Consulta de datos del empleados.", response = ResponseEntity.class, httpMethod = "GET", authorizations = {
//			@Authorization(value = "apiKey") })
//	@GetMapping(value = "/empleados")
//	public ResponseEntity<?> getEmpleados(@RequestHeader("id") String id) {
//		return new ResponseEntity<Object>(empleadosService.getEmpleados(id), HttpStatus.OK);
//	}
	@GetMapping(value = "/youtube")
	public ResponseEntity<?> youTube() {
		return new ResponseEntity<Object>(empleadosService.getDataYouTube(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/consultas")
	public ResponseEntity<?> consultas() {
		return new ResponseEntity<Object>(iConsultaService.consulta(), HttpStatus.OK );
	}
	
	@GetMapping(value = "/consultaId")
	public ResponseEntity<?> consultaId(@RequestHeader("id") int id) {
		return new ResponseEntity<Object>(iConsultaService.consultaID(id), HttpStatus.OK );
	}
	
	@PostMapping(value = "/insert")
	public ResponseEntity<?> insert(@RequestBody Sessions cliente ) {
		return new ResponseEntity<Object>(iConsultaService.insert(cliente), HttpStatus.OK );
	}
	
	
	@GetMapping(value = "/youtube1")
	public ResponseEntity<?> youTube1() {
		return new ResponseEntity<Object>(empleadosService.getDataYouTube(), HttpStatus.OK);
	}
	
}