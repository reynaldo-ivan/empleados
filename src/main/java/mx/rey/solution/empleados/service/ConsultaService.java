package mx.rey.solution.empleados.service;

import java.util.HashMap; 
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.rey.solution.empleados.domain.Sessions;
import mx.rey.solution.empleados.model.SessionModel;
import mx.rey.solution.empleados.repository.ISesionesRepository;

@Service
public class ConsultaService implements IConsultaService {
	
	@Autowired
	private ISesionesRepository iSesionesRepository;

	@Override
	public List<Sessions> consulta() {
		Map<String, Object> data = new HashMap<>();
		data.put("data", "data");
		data.put("data1", "data1"); 
		List<Sessions> lista =  iSesionesRepository.findAll();
		return lista;
	}

	@Override
	public Optional<Sessions> consultaID(int id) {
		Optional<Sessions>data =iSesionesRepository.findById(id);
		return data;
	}

	@Override
	public Boolean insert(Sessions cliente) {
		iSesionesRepository.save(cliente);
		return true;
	}

}
