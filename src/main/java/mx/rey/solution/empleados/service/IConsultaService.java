package mx.rey.solution.empleados.service;

import java.util.List;
import java.util.Optional;

import mx.rey.solution.empleados.domain.Sessions;
import mx.rey.solution.empleados.model.SessionModel;

public interface IConsultaService {
	
	List<Sessions> consulta ();

	Optional<Sessions> consultaID (int id);
	

	Boolean insert (Sessions cliente);

}
