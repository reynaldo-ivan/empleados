 
package mx.rey.solution.empleados.service;
 
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.jboss.jandex.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import mx.rey.solution.empleados.util.EmpleadosConstantes;  
 
@Service
public class EmpleadosConsumeService implements IEmpleadosConsumeService {
	
	@Autowired
	RestTemplate restTemplate;
 
    /** La Constante LOGGER. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadosConsumeService.class);
  
	@SuppressWarnings({ "unchecked" })
//	@Async  CompletableFuture<Object>
    public Map<String, Object> requestWsTrx(String url,HttpMethod httpMethod, String logDescripcionOk,
            String logDescripcionError, HttpEntity<Object> entityRetry) {
    	ResponseEntity<Object> response = exchange(url, 
    			httpMethod,entityRetry ); 
        return (Map<String, Object>) response.getBody(); 
//        CompletableFuture.completedFuture( responseMap )
    }
	
	
	@SuppressWarnings({ "unchecked" })
	@Async  
    public CompletableFuture<Object> requestWs(String url,HttpMethod httpMethod, String logDescripcionOk,
            String logDescripcionError, HttpEntity<Object> entityRetry) {
    	ResponseEntity<Object> response = exchange(url,httpMethod,entityRetry ); 
    	Map<String, Object>responseMap = (Map<String, Object>) response.getBody(); 
      return  CompletableFuture.completedFuture(responseMap );
    }
	 
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> consumeYouTube(String url, HttpMethod httpMethod, String logDescripcionOk,
			String logDescripcionError, HttpEntity<Object> entityRetry) {
		ResponseEntity<Object> response = exchange(url,httpMethod, entityRetry);
 
		return (Map<String, Object>) response.getBody();
	}
 
	
	private ResponseEntity<Object> exchange(String url, HttpMethod metodo,
			HttpEntity<Object> entityRetry  ) {
		
		ResponseEntity<Object> entitys = new ResponseEntity<>( HttpStatus.BAD_REQUEST ); 
		  
        try { 
        	 entitys = restTemplate.exchange(url, metodo, entityRetry, Object.class);  
        } catch (RestClientException e) {
        	LOGGER.error(EmpleadosConstantes.ERROR,e);
        }
		return entitys;
	}


 
	 
}
