 
package mx.rey.solution.empleados.service;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.http.HttpEntity; 
import org.springframework.http.HttpMethod; 
 
public interface IEmpleadosConsumeService {
  
//	CompletableFuture<Object> 
	Map<String, Object> requestWsTrx( String url,HttpMethod httpMethod, String logDescripcionOk,
            String logDescripcionError, HttpEntity<Object> entityRetry);
	
	CompletableFuture<Object> requestWs(String url,HttpMethod httpMethod, String logDescripcionOk,
            String logDescripcionError, HttpEntity<Object> entityRetry);
	
	Map<String, Object> consumeYouTube(String url,HttpMethod httpMethod, String logDescripcionOk,
            String logDescripcionError, HttpEntity<Object> entityRetry);
 
}
