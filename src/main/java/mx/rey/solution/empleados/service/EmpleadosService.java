
package mx.rey.solution.empleados.service;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service; 
import mx.rey.solution.empleados.util.EmpleadosConstantes;  

 
@Service
public class EmpleadosService implements IEmpleadosService {

	/** La Constante LOGGER. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadosService.class);
 
	@Autowired 
	private IEmpleadosConsumeService serviceBaseService;
  
	 
//	@SuppressWarnings("unchecked")
//	@Override
//	public ArrayList<Map<String , Object>>  getEmpleados(String key ) {
//		CompletableFuture<Object> dataPE68 = serviceBaseService.requestWsTrx("URL", HttpMethod.GET, 
//				"","",new HttpHeaders());
//		 
//		Map<String, Object> body = new HashMap<>();  
//		HttpHeaders headers = new HttpHeaders();
//		HttpEntity<Object> entityRetry = new HttpEntity<Object>(body, headers); 
//		CompletableFuture<Object> rest = (CompletableFuture<Object>) serviceBaseService.requestWs("http://localhost:9090/clientes/consulta",
//				HttpMethod.GET, EmpleadosConstantes.OK,EmpleadosConstantes.ERROR,entityRetry);
//		   
//		CompletableFuture<Object> rest1 = (CompletableFuture<Object>) serviceBaseService.requestWs("http://localhost:9090/clientes/consulta",
//				HttpMethod.GET, EmpleadosConstantes.OK,EmpleadosConstantes.ERROR,entityRetry);
//		
//		
//		//Espera a que terminen las dos peticiones.  
//		CompletableFuture.allOf(rest,rest1).join();
//		
//		Map<String, Object> responseData1 = new HashMap<String, Object>();
//		Map<String, Object> responseData2 = new HashMap<String, Object>();
//		try {
//			responseData1 = (Map<String, Object>) rest.get(); 
//			responseData2 = (Map<String, Object>) rest1.get();
//		} catch (InterruptedException | ExecutionException e) {
//			LOGGER.error(EmpleadosConstantes.ERROR,e);
//		}  
//		ArrayList<Map<String , Object>> list = new ArrayList<>();
//		list.add(responseData1);
//		list.add(responseData2);
//		return list; 
//	}


	@Override
	public Map<String, Object> getDataYouTube() {
		HttpEntity<Object> entityRetry = new HttpEntity<Object>(null);
		return serviceBaseService.consumeYouTube("https://www.googleapis.com/youtube/v3/videos?id=7lCDEYXw3mM&key=AIzaSyBIuRo5f-6xOZC3pSbNfHGmf9esDLl0FwI&part=snippet,contentDetails,statistics,status", 
				HttpMethod.GET, "OK","ERROR",entityRetry); 
		
	}
	
	
	
	

	

	
//	Map<String, Object> bodyAlta = new HashMap<>();  
//	HttpHeaders headersAlta = new HttpHeaders();
//	headersAlta.add("nombre", rest.get("nombre").toString()); 
//	headersAlta.add("ap", rest.get("ap").toString()); 
//	headersAlta.add("am", rest.get("am").toString()); 
//	
//	HttpEntity<Object> entityRetryAlta = new HttpEntity<Object>(bodyAlta, headersAlta); 
//	return serviceBaseService.requestWsTrx("http://localhost:9090/clientes/alta",
//			HttpMethod.POST, DashboardsConstantes.OK,DashboardsConstantes.ERROR,entityRetryAlta);
	  
 
   
}
