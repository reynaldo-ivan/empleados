package mx.rey.solution.empleados.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.rey.solution.empleados.domain.Sessions; 
 
@Repository
public interface ISesionesRepository extends JpaRepository<Sessions, Integer> {

}