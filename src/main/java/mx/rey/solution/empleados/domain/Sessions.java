package mx.rey.solution.empleados.domain;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table; 

import javax.persistence.Column;
import javax.persistence.Entity;
  
@Entity
@Table(name = "SESSIONS_CLIENT1")
public class Sessions implements Serializable {

	private static final long serialVersionUID = 1149475227529513482L;
 
	@Id
	@Column(name = "COD_CLIENT")
	private Integer codClient;
 
	@Column(name = "NOM_CLIENT")
    private String nomClient;
	
	@Column(name = "AP_CLIENT")
    private String apClient;
	
	@Column(name = "AM_CLIENT")
    private String amClient;

	@Column(name = "VALIDATE_CLIENT")
    private Boolean validateClient;

	public Integer getCodClient() {
		return codClient;
	}

	public void setCodClient(Integer codClient) {
		this.codClient = codClient;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getApClient() {
		return apClient;
	}

	public void setApClient(String apClient) {
		this.apClient = apClient;
	}

	public String getAmClient() {
		return amClient;
	}

	public void setAmClient(String amClient) {
		this.amClient = amClient;
	}

	public Boolean getValidateClient() {
		return validateClient;
	}

	public void setValidateClient(Boolean validateClient) {
		this.validateClient = validateClient;
	}
 	
	
	
}

