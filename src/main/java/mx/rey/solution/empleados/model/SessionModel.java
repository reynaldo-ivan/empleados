package mx.rey.solution.empleados.model;

public class SessionModel { 
	private Integer codClient;
  
    private String nomClient; 
    private String apClient;
	 
    private String amClient; 
    private Boolean validateClient;

	public Integer getCodClient() {
		return codClient;
	}

	public void setCodClient(Integer codClient) {
		this.codClient = codClient;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getApClient() {
		return apClient;
	}

	public void setApClient(String apClient) {
		this.apClient = apClient;
	}

	public String getAmClient() {
		return amClient;
	}

	public void setAmClient(String amClient) {
		this.amClient = amClient;
	}

	public Boolean getValidateClient() {
		return validateClient;
	}

	public void setValidateClient(Boolean validateClient) {
		this.validateClient = validateClient;
	}
}
